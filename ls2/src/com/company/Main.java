package com.company;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        //region Exceptions
        System.out.println("Started");

        int[] array = new int[]{1,2,3,4,5};

        try {
            for (int i = 0; i < 5; i++) {
                int temp = array[i];
                System.out.println("Number: " + temp);

            }
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
            System.out.println("Catch block");
        }
        finally {
            System.out.println("Finally will work in any case");
        }
        System.out.println("FINISH");
        //endregion
    }
}
