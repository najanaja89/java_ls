package com.kz.itsoft;

import java.sql.*;

public class PosgresDBExample {
    public static void main(String[] args) {
       String url ="jdbc:postgresql://localhost:5432/postgres";
       String user = "";
       String password ="";

        System.out.print("try to connect ");
        try (Connection con = DriverManager.getConnection(url, user, password);
            PreparedStatement pst = con.prepareStatement("SELECT * from public.persons");
            ResultSet rs = pst.executeQuery();
            )
        {
            while(rs.next()){
                System.out.print(rs.getInt(1));
                System.out.print(": ");
                System.out.println(rs.getString(2));
            }
        }
        catch(SQLException ex){
            System.out.println(ex.getErrorCode());
            ex.getStackTrace();
        }

    }
}
