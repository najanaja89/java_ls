package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
//	Thread(); //class
//	Runnable(); //interface

//        Thread thread1 = new Thread();
//        Thread thread2 = new Thread("Thread name");
        MyThreadTo myThreadTo = new MyThreadTo();
        MyThreadTo myThreadTo2 = new MyThreadTo();
        MyThreadTo myThreadTo3 = new MyThreadTo();
        myThreadTo.setName("Thread ONE");
        myThreadTo2.setName("Thread TWO");
        myThreadTo3.setName("Thread THREE");
        myThreadTo.start();
        try {
            myThreadTo.join(750);
        }
        catch (InterruptedException e){
            System.out.println("EX "+e.getMessage());
        }

        myThreadTo2.start();
        myThreadTo3.start();

//        MyThread myThread = new MyThread();
//        Thread thread = new Thread(myThread);
//        thread.start();

    }

    static class MyThreadTo extends  Thread {
        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                try {
                    Random random = new Random();
                    Thread.sleep(random.nextInt(500 - 1) + 1);
                    System.out.println("My Thread to " + this.getName() +" "+i);
                }
                catch (Exception ex)
                {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

    static class MyThread implements Runnable
    {
        @Override
        public void run(){
            System.out.println("My Thread");
        }
    }
}
