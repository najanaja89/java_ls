package com.company;

import com.company.employee.Employee;

public interface Worker extends Employee {
    @Override
    public void doTask() {

    }
}
