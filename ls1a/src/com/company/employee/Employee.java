package com.company.employee;

public class Employee {
    private int age;
    private String name;
    private String depID;
    private double salary;

    public Employee(int age, String name, String depID, double salary) {
        this.age = age;
        this.name = name;
        this.depID  = depID;
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public String getDepID() {
        return depID;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setDepID(String depID) {
        this.depID = depID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
